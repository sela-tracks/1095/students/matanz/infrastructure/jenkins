# install Jenkins instance on Kubernetes cluster


**Requirements**

* Installed kubectl command-line tool.

* Connected to a Kubernetes cluster - Have a kubeconfig file (default location is ~/.kube/config).

* Install helm.

**Adding the Jenkins chart repository**

Jenkins Helm charts are provided from https://charts.jenkins.io. To make this chart repository available, run the following commands:
```
helm repo add jenkins https://charts.jenkins.io
helm repo update
```

**Deploying a simple Jenkins instance**

To deploy a Jenkins instance with the default settings, run the command:
```
helm upgrade --install myjenkins jenkins/jenkins
```
in the output of the command above:
The first command listed in the notes returns the password for the admin user
The second command listed in the notes establishes a tunnel to the service in the Kubernetes cluster.

After the tunnel is established, open http://localhost:8080 on your local PC and you'll be directed to the Jenkins instance in the Kubernetes cluster. Login with the username admin and the password returned by the first command.

You now have a functional, if basic, Jenkins instance running in Kubernetes.
